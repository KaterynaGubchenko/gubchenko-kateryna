import Stackoverflow.LoginPage;
import Stackoverflow.MainPage;
import Stackoverflow.QuestionPage;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: gubchenko
 * Date: 6/22/16
 * Time: 8:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestStackoverflow {
    private static WebDriver driver;
    private static MainPage mainPage;
    private static String url;

    @BeforeClass
    public static void setUpBefore() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        url = "https://stackoverflow.com/";
        driver.get(url);
        driver.manage().window().maximize();
        mainPage = new MainPage(driver);
    }

    @AfterClass
    public static void teardownAfter() {
        driver.close();
    }

    @Test
    public void check_featured_numbert() {
        Assert.assertTrue("Значение больше 300", Integer.parseInt(mainPage.featured_number.getText()) > 300);
    }

    @Test
    public void check_login_options() {
        LoginPage login = mainPage.navigateToLoginPage();
        assertNotNull("Нет возможности войти через Google", login.log_with_google);
        assertNotNull("Нет возможности войти через Facebook", login.log_with_facebook);
        login.logoToMainPagefromLogin();
    }

    @Test
    public void check_question_date() {
        QuestionPage qpage = mainPage.navigateToQuestionPage();
        assertEquals("Вопрос задан НЕ сегодня", "today", qpage.asked_date.getText());
        qpage.logoToMainPage();
    }
}