import Rozetka.BucketPage;
import Rozetka.CitySelect;
import Rozetka.MainPage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: gubchenko
 * Date: 6/16/16
 * Time: 7:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestRozetka {
    private static WebDriver driver;
    private static MainPage mainPage;
    private static String url;

    @BeforeClass
    public static void setUpBefore() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        url = "http://rozetka.com.ua/";
        driver.get(url);
        driver.manage().window().maximize();
        mainPage = new MainPage(driver);
            }

    @AfterClass
    public static void teardownAfter() {
        driver.close();
    }

    @Test
    public void checkLogoRozetkaPresent() {
        assertNotNull("Логотип отсутствует", mainPage.logo);

    }

    @Test
    public void findAppleInCatalog() {
        assertNotNull("Яблоками не торгуем. Это для слабаков, которые боятся Китая", mainPage.apple);
    }

    @Test
    public void findMp3InCatalog() {
        assertNotNull("Нет секции, содержащей mp3", mainPage.mp3);
    }

    @Test
    public void checkCitiesKharkivPresentInCityChooser() {
        CitySelect city = mainPage.navigateToCityChooser();
        assertNotNull("Нет города под названием 'Киев'", city.cityKyiv);
        assertNotNull("Нет города под названием 'Харьков'", city.cityKharkiv);
        assertNotNull("Нет города под названием 'Одесса'", city.cityOdessa);
        city.closecity();
    }

    @Test
    public void checkBucketText() {
        BucketPage bucket = mainPage.navigateToBucket();
        assertNotNull("В корзине есть выбранные товары", bucket.bucket_contains_text);
        bucket.closebucket();

    }
}