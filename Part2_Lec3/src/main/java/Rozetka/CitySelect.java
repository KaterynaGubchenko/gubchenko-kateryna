package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created with IntelliJ IDEA.
 * User: gubchenko
 * Date: 6/16/16
 * Time: 7:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class CitySelect {
    private WebDriver driver;

    public CitySelect(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='city-chooser']//*[contains(text(), 'Киев')]")
    public WebElement cityKyiv;

    @FindBy(xpath = ".//*[@id='city-chooser']//*[contains(text(), 'Харьков')]")
    public WebElement cityKharkiv;

    @FindBy(xpath = ".//*[@id='city-chooser']//*[contains(text(), 'Одесса')]")
    public WebElement cityOdessa;


    @FindBy(xpath = ".//*[@class='popup-close']")
    public WebElement closecity;

    public void closecity() {
        closecity.click();
    }
}