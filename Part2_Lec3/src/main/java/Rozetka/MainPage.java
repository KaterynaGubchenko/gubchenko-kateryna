package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created with IntelliJ IDEA.
 * User: gubchenko
 * Date: 6/16/16
 * Time: 7:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@class='logo']")
    public WebElement logo;

    @FindBy(xpath = ".//*[@title='Apple']")
    public WebElement apple;

    @FindBy(xpath = ".//*[@class='m-main']//*[contains(text(), 'MP3')]")
    public WebElement mp3;

    @FindBy(xpath = ".//*[@class='novisited sprite-side header-city-select-link']")
    public WebElement lnk_city_chooser;

    @FindBy(xpath = ".//*[@href='https://my.rozetka.com.ua/cart/']")
    public WebElement lnk_bucket;

    public CitySelect navigateToCityChooser() {
        lnk_city_chooser.click();
        return new CitySelect(driver);
    }

    public BucketPage navigateToBucket() {
        lnk_bucket.click();
        return new BucketPage(driver);

    }


}
