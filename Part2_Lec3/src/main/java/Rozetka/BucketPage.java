package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created with IntelliJ IDEA.
 * User: gubchenko
 * Date: 6/16/16
 * Time: 7:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class BucketPage {
    private WebDriver driver;

    public BucketPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[text()='Корзина пуста']")
    public WebElement bucket_contains_text;

    @FindBy(xpath = ".//*[@src='http://i.rozetka.ua/design/_.gif']")
    public WebElement closebucket;

    public void closebucket() {
        closebucket.click();
    }


}
