package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created with IntelliJ IDEA.
 * User: gubchenko
 * Date: 6/22/16
 * Time: 7:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class QuestionPage {
    private WebDriver driver;

    public QuestionPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//*[@id='qinfo']//p[contains(text(), 'asked')] /../following-sibling::*//b")
    public WebElement asked_date;

    @FindBy(xpath = ".//*[@id='hlogo']/a")
    public WebElement logoToMainPage;

    public void logoToMainPage(){
      logoToMainPage.click();
   }
}

