package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created with IntelliJ IDEA.
 * User: gubchenko
 * Date: 6/22/16
 * Time: 7:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoginPage {
    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//*[@data-provider='google']/div[@class='text']")
    public WebElement log_with_google;

    @FindBy(xpath = ".//*[@data-provider='google']/div[@class='text']")
    public WebElement log_with_facebook;

    @FindBy(xpath = ".//*[@id='hlogo']/a")
    public WebElement logoToMainPagefromLogin;

    public void logoToMainPagefromLogin(){
        logoToMainPagefromLogin.click();

}
}