package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: gubchenko
 * Date: 6/22/16
 * Time: 7:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@class='bounty-indicator-tab']")
    public WebElement featured_number;


    @FindBy(xpath = ".//a[text()='sign up']")
    public WebElement lnk_login;

    public LoginPage navigateToLoginPage() {
        lnk_login.click();
        return new LoginPage(driver);
    }

    @FindBy(xpath = "//*[@id='question-mini-list']//h3/a")
    public List<WebElement> questions;

    public int findRandomQuestion() {
        int q = (int) (Math.random() * questions.size());
        return q;
    }

    public QuestionPage navigateToQuestionPage() {
        questions.get(findRandomQuestion()).click();
        return new QuestionPage(driver);
    }
}
