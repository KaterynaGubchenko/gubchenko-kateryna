package Lab2;

/**
 * Created by gubchenko on 4/8/2016.
 */

public class Task1 {
    public static void main(String[] args) {
        int x = 10;
        int y = 5;
        int z = 0;
        String temp;
        boolean bool=false;
        System.out.println("Converting to string and back:");
        temp = Integer.toString(x);
        System.out.println("int to String: " + temp);
        x=Integer.parseInt(temp);
        System.out.println("String to int: " + x);
        long longX = 100L;
        temp = Long.toString(longX);
        System.out.println("long to Sring: " + temp);
        longX=Long.parseLong(temp);
        System.out.println("String to long: " + longX);
        float floatX= 1.1F;
        temp=Float.toString(floatX);
        System.out.println("float to String: " + temp);
        floatX=Float.parseFloat(temp);
        System.out.println("String to float: " + floatX);
        double doubleX=1.1D;
        temp=Double.toString(doubleX);
        System.out.println("double to String: " + temp);
        doubleX=Double.parseDouble(temp);
        System.out.println("String to double: " + doubleX);
        char charX='a';
        temp=Character.toString(charX);
        System.out.println("char to String: " + temp);
        charX=temp.charAt(0);
        System.out.println("String to char: " + charX);
        temp=Boolean.toString(bool);
        System.out.println("bool to String: " + temp);
        bool=Boolean.parseBoolean(temp);
        System.out.println("String to bool: " + bool);
    }
    }

