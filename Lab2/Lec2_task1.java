package Lab2;

/**
 * Created by gubchenko on 4/8/2016.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
public class Task1 {
    public static void main(String[] args) {
        int a = 10;
        int b = 3;
        int c = 0;
              System.out.println("a=" + a + " b=" + b);
        System.out.println("  Arithmetic");
        c=a+b;
        System.out.println("a+b=" + c);
        c=a-b;
        System.out.println("a-b=" + c);
        c=a*b;
        System.out.println("a*b=" + c);
        c=a/b;
        System.out.println("a/b=" + c);
        c=a%b;
        System.out.println("a%b=" + c);

        System.out.println("  Compatison");
        boolean bool=false;
        bool=a>b;
        System.out.println("a>b=" + bool);
        bool=a<b;
        System.out.println("a<b=" + bool);
        System.out.println("  Logic");
        bool=a>b && a<20;
        System.out.println("a>b and a<20?  - " + bool);
        bool=a<b || a<20;
        System.out.println("a>b OR a<20?  - " + bool);
        System.out.println("  Increment");
        a++;
        System.out.println("a++=" + a);
        a=b;
        System.out.println("if a=b then a will be equal to:  " + a);

    }
}
