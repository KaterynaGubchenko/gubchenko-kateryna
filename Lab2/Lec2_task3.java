package Lab2;

/**
 * Created by gubchenko on 4/8/2016.
 */

public class Task1 {
    public static void main(String[] args) {
      int x =0;
        float floatX= 1.1F;

        System.out.println("Переведите число с точкой в тип без точки и наоборот");
        x=(int)floatX;
        System.out.println("float 1.1 to int = " + x);
        floatX=(float)x;
        System.out.println("int 1 to float = " + floatX);
    }
}
