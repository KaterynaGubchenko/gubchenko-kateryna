package Lab2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by gubchenko on 4/8/2016.
 */

public class Task1 {
    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter first number");
        float result=0;

        try{
            float firstNumber = Float.parseFloat(bufferedReader.readLine());
            System.out.println("enter arithmetic operation sign");
            char operator = bufferedReader.readLine().charAt(0);
            System.out.println("enter second number");
            float secondNumber = Float.parseFloat(bufferedReader.readLine());

            switch (operator){
                case '+':{
                    result=firstNumber+secondNumber;
                    if(result%1==0)
                        System.out.println("you result is " + (int)result);
                    else
                        System.out.println("you result is " + result);
                    break;
                }
                case '-': {
                    result=firstNumber-secondNumber;
                    if(result%1==0)
                        System.out.println("you result is " + (int)result);
                    else
                        System.out.println("you result is " + result);
                    break;
                }
                case '*':{
                    result=firstNumber*secondNumber;
                    if(result%1==0)
                        System.out.println("you result is " + (int)result);
                    else
                        System.out.println("you result is " + result);
                    break;
                }
                case '/':{
                    result=firstNumber/secondNumber;
                    if(result%1==0)
                        System.out.println("you result is " + (int)result);
                    else
                        System.out.println("you result is " + result);
                    break;
                }
                case '%':{
                    result=firstNumber%secondNumber;
                    if(result%1==0)
                        System.out.println("you result is " + (int)result);
                    else
                        System.out.println("you result is " + result);
                    break;
                }
                default:
                    System.out.println("system cannot recognize your arithmetic operation");
            }


        }
        catch (Exception e){
            System.out.println("you entered NOT number: " + e.getMessage());
        }



    }
}
