import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;

public class TestRozetka {
    private static WebDriver driver;
    private static String url;

    @BeforeClass
    public static void setUpBefore() throws Exception{
        driver=new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        url="http://rozetka.com.ua/";
        driver.get(url);
        driver.manage().window().maximize();
}

@Test
public void findlogo() throws Exception{

    WebElement logo=driver.findElement(By.xpath(".//*[@class='logo']"));
    assertNotNull("Логотип отсутствует", logo);
}

    @Test
    public void findapple() throws Exception{

        WebElement apple=driver.findElement(By.xpath(".//*[@title='Apple']"));
        assertNotNull("Яблоками не торгуем. Это для слабаков, которые боятся Китая", apple);
    }

    @Test
    public void findsectionwithmp3() throws Exception{

        WebElement section=driver.findElement(By.xpath(".//*[@class='m-main']//*[contains(text(), 'MP3')]"));
        assertNotNull("Нет секции, содержащей mp3", section);
    }

    @Test
    public void city() throws Exception{

        WebElement choosecity=driver.findElement(By.xpath(".//*[@id='city-chooser']/a"));
        assertNotNull("Невозможно найти элемент 'Выберите город'", choosecity);
                       choosecity.click();
        WebElement cityKyiv=driver.findElement(By.xpath(".//*[@id='city-chooser']//*[contains(text(), 'Киев')]"));
        assertNotNull("Нет города под названием 'Киев'", cityKyiv);
        WebElement cityKharkiv=driver.findElement(By.xpath(".//*[@id='city-chooser']//*[contains(text(), 'Харьков')]"));
        assertNotNull("Нет города под названием 'Харьков'", cityKharkiv);
        WebElement cityOdessa=driver.findElement(By.xpath(".//*[@id='city-chooser']//*[contains(text(), 'Одесса')]"));
        assertNotNull("Нет города под названием 'Одесса'", cityOdessa);
           WebElement close=driver.findElement(By.xpath(".//*[@src='http://i.rozetka.ua/design/_.gif']"));
        close.click();

    }
    @Test
    public void bucket() throws Exception{
        WebElement findbucket=driver.findElement(By.xpath(".//*[@href='https://my.rozetka.com.ua/cart/']"));
        assertNotNull("Корзина не найдена", findbucket);

        findbucket.click();

        WebElement pusta=driver.findElement(By.xpath(".//*[text()='Корзина пуста']"));
        assertNotNull("В корзине есть выбранные товары", pusta);
        WebElement closebucket=driver.findElement(By.xpath(".//*[@src='http://i.rozetka.ua/design/_.gif']"));
        closebucket.click();
                    }


    @AfterClass
    public static void  teardownAfter() throws Exception{
        driver.close();
    }

}