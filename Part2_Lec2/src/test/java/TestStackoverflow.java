import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by kacka_ch on 11.06.2016.
 */
public class TestStackoverflow {
    private static WebDriver driver;
    private static String url;

    @BeforeClass
    public static void setUpBefore() throws Exception{
        driver=new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        url="http://stackoverflow.com/";
        driver.get(url);
        driver.manage().window().maximize();

    }
    @Before
    public void setUp() throws Exception {
        if (driver == null) {
            setUpBefore();
        } else {
            if (!driver.getCurrentUrl().equals(url)) {
                driver.get(url);
            }
        }
    }

    @Test
    public void featured() throws Exception {
        WebElement featurednumber=driver.findElement(By.xpath(".//*[@class='bounty-indicator-tab']")) ;
        String count=featurednumber.getText();
        int number=Integer.parseInt(count);
        Assert.assertTrue("Значение больше 300", number>300);
    }

    @Test
    public void signup() throws Exception{
        WebElement sign=driver.findElement(By.xpath(".//a[text()='sign up']")) ;
        sign.click();
        WebElement but_google=driver.findElement(By.xpath(".//*[@data-provider='google']/div[@class='text']")) ;
        assertNotNull("Нет возможности войти через Google", but_google);
        WebElement but_facebook=driver.findElement(By.xpath(".//*[@data-provider='facebook']/div[@class='text']")) ;
        assertNotNull("Нет возможности войти через Facebook", but_facebook);

    }
    @Test
    public void today_question() throws Exception{
        List<WebElement> questions = driver.findElements(By.xpath(".//*[@id='question-mini-list']//h3/a"));
        int q = (int) (Math.random()*questions.size());
        questions.get(q).click();
        WebElement askedDate = driver.findElement(By.xpath(".//*[@id='qinfo']//p[contains(text(), 'asked')] /../following-sibling::*//b"));
        assertEquals("Вопрос задан НЕ сегодня", "today", askedDate.getText());




    }


    @AfterClass
    public static void  teardownAfter() throws Exception{
        driver.close();
    }
}