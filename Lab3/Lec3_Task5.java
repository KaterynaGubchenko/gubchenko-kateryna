package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        int summa =0;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Вводите числа для суммирования, для вычисления суммы введите слово 'сумма'");
            while (true) {
                String s = bufferedReader.readLine();
                if (s.equals("сумма")) {
                    System.out.println("Итоговая сумма равна: "+ summa);
                    break;
                }

                int s1 = Integer.parseInt(s);
                summa+= s1;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}