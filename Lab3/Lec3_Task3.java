package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
//Вывести все простые числа от 1 до N. N задается пользователем через консоль

public class Task1 {

    public static void main(String[] args) throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Введите диапазон, до которого искать простые числа: ");
        int range=Integer.parseInt(br.readLine());
        System.out.println();
        for(int i=2;i<=range;i++)
        {
            int flag=0;
            for(int j=2;j<i;j++)
            {
                if(i%j==0)
                {
                    flag=1;
                }
            }
            if(flag==0)

                System.out.print(i+" ");
        }
               System.out.println();

    }
}
